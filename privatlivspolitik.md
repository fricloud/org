# Privatlivspolitik
Denne politik gælder fra %DATO%.

## Introduktion
Fricloud er en forening sat i verdenen for at du kan holde din data sikker. Dette dokument er din garanti for at vi opretholder det løfte.

## Information vi får om dig
Hver tjeneste som vi hoster på vores servere indsamler forskellige mængder af data.
### Alle
Når du sender en request til serveren, (klikker på et link f.eks.) bliver stien du beder om, din IP addresse, din user agent, statuskoden, og content-length logget.
### Nextcloud
* Alle requests som sendes igennem nextcloud klienten logger også dit brugernavn
* Alt du med vilje uploader bliver gemt indtil du sletter det. Det betyder
### Matrix
 *
### Email
 *
### Gitea
 *

## Når du deler din data
Hvis du

## Dine rettigheder
### Retten til at blive glemt
Når du sletter din konto hos fricloud.dk, spørger vi dig om at du gerne vil downloade din data. Det download-link vil være aktivt i en måned, og vil være beskyttet med dit kodeord. Efter den uge vil vi slette alt data omkring dig, undtagen det brugernavn som du havde. Det er for at undgå at en anden person kan imitere dig efter, og for eksempel modtage emails der er målrettet mod dig.

Du kan gøre dette fra dine kontoindstillinger.
## Få alt din data
Du har retten til at få alt din data nedhentet. Det kan gøres fra dine kontoindstillinger

## Ændringer
Ændringer i privatlivspolitikken vil blive vedtaget på generalforsamlinger. Derefter vil der gå mindst en måned inden den træder i kraft.

